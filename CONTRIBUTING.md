# Contribiting to my NLP portafolio

First off, thank you for considering contributing to my NLP portafolio. It's people like you that make my NLP portafolio such a great portfolio project!

## Code of Conduct

This project and everyone participating in it is governed by the my NLP portafolio Code of Conduct. By participating, you are expected to uphold this code. Please report unacceptable behavior to otto.hahn.herrera (at) gmail.com.

## I don't want to read this whole thing I just have a question!!!

If you have questions about how to contribute to my NLP portafolio, feel free to send me an email at otto.hahn.herrera (at) gmail.com, or file an issue [link to GitLab issue tracker]. 

## What should I know before I get started?

### Project Structure

[Provide a brief overview of your portfolio project's structure, mentioning the main directories and how they are organized, with a focus on Python project specifics.]

### Code of Conduct

Our Pledge: In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone.

## How Can I Contribute?

### Reporting Bugs

This section guides you through submitting a bug report for my NLP portafolio. Following these guidelines helps maintainers and the community understand your report, reproduce the behavior, and find related reports.

**Before Submitting A Bug Report**

* Check the documentation for a list of common questions and problems.
* Ensure the bug was not already reported by searching on GitLab under [link to issues].

**How Do I Submit A Good Bug Report?**

Bugs are tracked as [GitLab issues](link to GitLab issues). Explain the problem and include additional details to help maintainers reproduce the problem:

* Use a clear and descriptive title for the issue to identify the problem.
* Describe the exact steps which reproduce the problem in as many details as possible.
* Provide specific examples to demonstrate the steps.

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for my NLP portafolio, including completely new features and minor improvements to existing functionality.

**Before Submitting An Enhancement Suggestion**

* Check if you're using the latest version and if you can get the desired behavior by changing my NLP portafolio settings.
* Search [GitLab issues](link to GitLab issues) to see if the enhancement has already been suggested.

**How Do I Submit A Good Enhancement Suggestion?**

Enhancement suggestions are tracked as GitLab issues. Provide the following information:

* Use a clear and descriptive title for the issue to identify the suggestion.
* Provide a step-by-step description of the suggested enhancement in as many details as possible.
* Provide specific examples to illustrate the steps or provide mock-ups or examples if possible.

### Your First Code Contribution

Unsure where to begin contributing to my NLP portafolio? You can start by looking through these `beginner` and `help-wanted` issues:

* Beginner issues - issues which should only require a few lines of code, and a test or two.
* Help wanted issues - issues which should be a bit more involved than `beginner` issues.

### Merge Requests

The process described here has several goals:

- Maintain my NLP portafolio's quality
- Fix problems that are important to users
- Engage the community in working toward the best possible my NLP portafolio
- Enable a sustainable system for my NLP portafolio's maintainers to review contributions

Please follow these steps to have your contribution considered by the maintainers:

1. Follow all instructions in [the template](link to merge request template)
2. Follow the [styleguides](#styleguides)
3. After you submit your merge request, verify that all [status checks](https://docs.gitlab.com/ee/user/project/merge_requests/) are passing

## Styleguides

### Git Commit Messages

* Use the present tense ("Add feature" not "Added feature")
* Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
* Limit the first line to 72 characters or less
* Reference issues and merge requests liberally after the first line

### Python Styleguide

* Follow the [PEP 8](https://www.python.org/dev/peps/pep-0008/) style guide for Python code.
* Use [Docstrings](https://www.python.org/dev/peps/pep-0257/) for code documentation.
* Ensure your code is compatible with the Python version stated in the project's documentation.

### Additional Notes

### Issue and Merge Request Labels

[If you use issue and merge request labels on GitLab, describe them here]

Thank you for reading through the CONTRIBUTING.md guide! We're excited to welcome you aboard and look forward to your contributions.
``
