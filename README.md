
# NLP Portfolio by Otto Hahn

Welcome to my Natural Language Processing (NLP) portfolio, my name is Otto Hahn and I am a data scientist with a passion for unlocking the potential of text through cutting-edge linguistic models and algorithms. This portfolio is a comprehensive showcase of my expertise in NLP, featuring a variety of tools and systems developed to address some of the most challenging and intriguing problems in language processing.

## Overview

The field of NLP has seen remarkable advancements, enabling machines to understand, interpret, and generate human language in ways previously unimaginable. This portfolio represents a collection of my work, demonstrating my ability to implement and innovate within this dynamic field. The examples included here span from fundamental text processing techniques to complex models for understanding and generating language.

### Featured Projects

- [X] **Tokenizer Functions**: A foundational tool for breaking down text into manageable pieces, or tokens, setting the stage for deeper linguistic analysis.

- **POS Tagger**: Utilizing machine learning to assign part of speech tags to each word in a sentence, facilitating syntactic analysis and understanding sentence structure.

- **NER Tagger**: A Named Entity Recognition system capable of detecting and classifying key elements in text into predefined categories such as person names, organizations, locations, etc.

- **Lemmatizer**: Sophisticated algorithm for reducing words to their base or dictionary form, an essential step in standardizing text input and improving the performance of various NLP tasks.

- **Stemmer**: A method for trimming words down to their stem, thereby reducing the complexity of linguistic analysis while maintaining the core meaning of words.

- [X] **Stopword Remover**: Essential for filtering out common words that add little semantic value to the analysis, streamlining the processing of text data.

- **Summarizer using TF-IDF**: An advanced tool for extracting key information from large texts, generating concise summaries based on the Term Frequency-Inverse Document Frequency method.

- **Topic Detection using Dirichlet Allocation**: Implements Latent Dirichlet Allocation (LDA) for uncovering the underlying topics within large collections of text, revealing hidden thematic structures.

- **Sentiment Analysis**: A model that interprets and classifies the emotional tone behind words, enabling the automatic monitoring of sentiment in customer feedback, social media, and beyond.

- **Language Detection by Letter Frequency**: An innovative approach to identifying the language of a text based on the frequency distribution of letters, showcasing the power of statistical analysis in NLP.

- **4-Gram Language Model**: A probabilistic model that predicts the likelihood of a sequence of words, based on the observation of four-word combinations, enhancing machine understanding of language context.

- **Bayesian Network Language Model**: An advanced probabilistic model that captures linguistic relationships and dependencies, offering robust predictions of word sequences.

- **Spell Checker**: A critical tool for identifying and correcting spelling errors, improving text quality and readability.

- **Regex Library**: I have compiled a regular expression library for useful stuff over the years, some of the expressions are mine, some are from different places across the web. I trey to attribute each one but, many are too old.

- **SMILES language parser**: A SMILES parser, that converts a SMILES entry into an XYZ file.

- **A library of LLM prompts**: A series of prompts to use in ChatGPT for different tasks. 

## Getting Started

To get started with this portfolio, please ensure you have Python installed on your system, as all projects are developed using Python. Clone this repository to your local machine, navigate to the project of interest, and follow the setup instructions provided within each project's directory. Also, be sure to check the wiki for theoretical explanations, references and detailed instructions.

## Contributing

Contributions to this portfolio are welcomed and appreciated. Whether it's a bug fix, a new feature, or an improvement to existing algorithms, your input is valuable. Please refer to the [CONTRIBUTING.md](./CONTRIBUTING.md) file for guidelines on how to contribute effectively.

## Contact

Should you have any questions, suggestions, or wish to contribute to the portfolio, please feel free to reach out to me directly.

Thank you for visiting my NLP portfolio. I look forward to advancing the field of natural language processing together.
