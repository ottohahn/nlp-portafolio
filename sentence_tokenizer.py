#!/usr/bin/env python3

"""
Sentence tokenizer functions. 
"""

def simple_sentence_tokenizer(text):
    """
    A simple sentence tokenizer that splits text into sentences based on
    newline characters and end of sentence punctuation (.!?), taking into account
    multiline input, decimal numbers, and common abbreviations.
    """
    import re
    
    # Pattern to match sentence endings, considering decimal numbers and abbreviations
    pattern = r"""
        (?<!\w\.\w.)                   # Negative lookbehind for word char, dot, word char (abbreviations)
        (?<![A-Z][a-z]\.)              # Negative lookbehind for single letter abbreviations, e.g., "E.g."
        (?<=\.|\?|!|\n)                # Positive lookbehind for sentence endings or new lines
        \s                             # Any whitespace character
        (?=[A-Z])                      # Positive lookahead for an uppercase letter (start of a new sentence)
    """
    
    sentences = re.split(pattern, text, flags=re.VERBOSE | re.MULTILINE)
    return [s.strip() for s in sentences if s.strip()]

# Example text to test the tokenizer
text = """
Dr. Smith goes to Washington. He will be there until 3:00 p.m. today. What about Mrs. Anderson?
She's visiting from the U.K. Isn't it great? Yes! And Dr. Brown, Ph.D., will join them at 10 a.m.
"""

# Tokenize the example text
tokenized_sentences = simple_sentence_tokenizer(text)
print(tokenized_sentences)
