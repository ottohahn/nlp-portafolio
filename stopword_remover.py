import re

def build_regex_from_file(file_path):
    """
    Reads a file containing stop words, one per line, and compiles a regex pattern
    that matches any of the stop words.
    """
    # Example stop words, replacing file reading in this environment
    stop_words = ["an", "is", "with", "some", "this"]

    # Create a regex pattern that matches any of the stop words
    # Pattern will look like r'\b(word1|word2|word3)\b'
    regex_pattern = r'\b(' + '|'.join(stop_words) + r')\b'
    return re.compile(regex_pattern, re.IGNORECASE)

def remove_stop_words(text, regex):
    """
    Removes all occurrences of stop words in the given text based on the compiled regex.
    """
    # Substitute found stop words with an empty string
    text = regex.sub("", text)
    # Replace multiple spaces with a single space
    return re.sub(r'\s+', ' ', text).strip()

if __name__ == '__main__':
    # Assume we have built the regex from a hypothetical file
    stop_words_regex = build_regex_from_file("stopwords_srt_unq.txt")

    # Example text
    text = "This is an example text with some common stop words removed."

    # Removing stop words from the text
    clean_text = remove_stop_words(text, stop_words_regex)
    print(clean_text)

